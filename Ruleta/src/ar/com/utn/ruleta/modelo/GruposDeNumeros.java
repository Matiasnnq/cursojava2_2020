package ar.com.utn.ruleta.modelo;

public class GruposDeNumeros {
	public static final int NEGRO 			=100;
	public static final int ROJO 			=200;
	public static final int PAR 			=300;
	
	public static final int IMPAR 			=400;
	public static final int PRIMERDOCENA	=500;
	public static final int SEGUNDADOCENA	=600;

	public static final int TERCERDOCENA	=700;
	public static final int FROM1TO18 		=800;	
	public static final int FROM19TO36 		=900;	

}
