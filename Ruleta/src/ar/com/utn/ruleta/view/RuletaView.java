package ar.com.utn.ruleta.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JScrollBar;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;

public class RuletaView {

	private JFrame frame;
	private JTextField textNumero;
	private JTextField textColor;
	private JTable tblApuesta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RuletaView window = new RuletaView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RuletaView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1000, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblRuleta = new JLabel("Ruleta");
		lblRuleta.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 30));
		lblRuleta.setBounds(242, 22, 109, 37);
		frame.getContentPane().add(lblRuleta);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(RuletaView.class.getResource("/ar/com/utn/ruleta/view/imagenes/ruleta.jpg")));
		lblNewLabel.setBounds(163, 96, 334, 151);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNumero = new JLabel("Numero");
		lblNumero.setBounds(104, 247, 46, 14);
		frame.getContentPane().add(lblNumero);
		
		JComboBox cmbNumero = new JComboBox();
		cmbNumero.setModel(new DefaultComboBoxModel(new String[] {"0", "1", "2", "3"}));
		cmbNumero.setBounds(71, 302, 65, 20);
		frame.getContentPane().add(cmbNumero);
		
		JButton btnApostarNumero = new JButton("Apostar");
		btnApostarNumero.setBounds(61, 333, 89, 23);
		frame.getContentPane().add(btnApostarNumero);
		
		JLabel lblColor = new JLabel("Color");
		lblColor.setBounds(207, 247, 46, 14);
		frame.getContentPane().add(lblColor);
		
		JRadioButton rdbtnRojo = new JRadioButton("Rojo");
		rdbtnRojo.setBounds(194, 301, 54, 23);
		frame.getContentPane().add(rdbtnRojo);
		
		JRadioButton rdbtnNegro = new JRadioButton("Negro");
		rdbtnNegro.setBounds(253, 301, 65, 23);
		frame.getContentPane().add(rdbtnNegro);
		
		JButton btnApostarColor = new JButton("Apostar");
		btnApostarColor.setBounds(191, 333, 89, 23);
		frame.getContentPane().add(btnApostarColor);
		
		JButton btnTirarBola = new JButton("Tirar bola");
		btnTirarBola.setBounds(326, 333, 89, 23);
		frame.getContentPane().add(btnTirarBola);
		
		textNumero = new JTextField();
		textNumero.setBounds(71, 271, 86, 20);
		frame.getContentPane().add(textNumero);
		textNumero.setColumns(10);
		
		JLabel lblSaldo = new JLabel("Saldo");
		lblSaldo.setBounds(10, 274, 46, 14);
		frame.getContentPane().add(lblSaldo);
		
		textColor = new JTextField();
		textColor.setColumns(10);
		textColor.setBounds(194, 271, 86, 20);
		frame.getContentPane().add(textColor);
		
		JButton btnLi = new JButton("Limpiar Apuesta");
		btnLi.setBounds(41, 367, 116, 23);
		frame.getContentPane().add(btnLi);
		
		JLabel lblApuesta = new JLabel("Apuesta");
		lblApuesta.setBounds(644, 111, 46, 14);
		frame.getContentPane().add(lblApuesta);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(540, 151, 150, 151);
		frame.getContentPane().add(scrollPane);
		
		tblApuesta = new JTable();
		tblApuesta.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
			},
			new String[] {
				"Apuesta", "Monto"
			}
		));
		scrollPane.setViewportView(tblApuesta);
	}
}
